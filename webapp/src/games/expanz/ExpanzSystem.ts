import { LinearMatrix } from "./../lib/LinearMatrix";
import { Vector2 } from "./../lib/Vector2";
import { AGameSystem } from "./../AGameSystem";
import { ExpanzGame } from "./ExpanzGame";
import { Tile } from "./Tile";
import { TileType } from "./TileType";

type MetaLinearMatrix = {width:number, height:number, data:TileType[]};

export class ExpanzSystem extends AGameSystem
{
    private readonly board:LinearMatrix<Tile>;
    
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        this.board = ExpanzSystem.convertDataToMatrix(data.board);
    }
    
    private static convertDataToMatrix(data:MetaLinearMatrix):LinearMatrix<Tile>
    {
        const tiles:Tile[] = [];
        let pos:Vector2;
        let output:LinearMatrix<Tile>;

        Tile.size.x = ~~(ExpanzGame.WIDTH / data.width);
        Tile.size.y = ~~(ExpanzGame.HEIGHT / data.height);
        for (let i = 0, len = data.data.length; i < len; i++) {
            pos = {x: i % data.width, y: ~~(i / data.width)};
            tiles.push(new Tile(pos, data.data[i]));
        }
        output = new LinearMatrix({
            width: data.width,
            height: data.height,
            data: tiles
        });
        return (output);
    }

    protected update = (data:any) =>
    {
        data.territory.forEach((v:Vector2) => {
            const tile = this.getTile(v.x, v.y);
            
            tile.isCaptured = true;
            tile.color = data.color;
            this.board.set(v.x, v.y, tile);
        });
    }
    protected end = (data:any) =>
    {

    }
    public forEach(callback:(t:Tile, x:number, y:number) => void)
    {
        this.board.forEach(callback);
    }
    public getTile(x:number, y:number):Tile
    {
        return (this.board.get(x, y)!);
    }
}