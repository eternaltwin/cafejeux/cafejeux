import { ADrawer } from "./../ADrawer";
import { Mouse } from "./../Mouse";
import { Vector2 } from "./../lib/Vector2";
import { Tile } from "./Tile";
import { TileType } from "./TileType";

export class ExpanzDrawer extends ADrawer
{
    public preload():void
    {
        const BASE = `${process.env.PUBLIC_URL}/assets/expanz`;

        for (let i = 1; i <= TileType.COUNT; i++) {
            this.addImage(i - 1, `${BASE}/${i}.png`);
        }
    }
    public getMousePosition():Vector2
    {
        return ({
            x: ~~(Mouse.x / Tile.size.x),
            y: ~~(Mouse.y / Tile.size.y)
        });
    }
    public drawTile(tile:Tile):void
    {
        Tile.image = this.getImage(tile.color);
        tile.draw(this.ctx);
    }
}