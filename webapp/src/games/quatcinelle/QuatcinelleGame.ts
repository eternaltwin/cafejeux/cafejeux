import { AGame } from "./../AGame";
import { Mouse } from "./../Mouse";
import { QuatcinelleDrawer } from "./QuatcinelleDrawer";
import { QuatcinelleSystem } from "./QuatcinelleSystem";

export class QuatcinelleGame extends AGame<QuatcinelleSystem, QuatcinelleDrawer>
{
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(QuatcinelleSystem, QuatcinelleDrawer, socket, data);
        Mouse.onClick(() => {
            this.system.emit("play", {
                position: this.helper.getMousePosition(),
            });
        });
    }
    
    public draw(ctx:CanvasRenderingContext2D)
    {
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, QuatcinelleGame.WIDTH, QuatcinelleGame.HEIGHT);
        if (!this.system.hasEnded) {
            this.helper.drawBackground();
            ctx.translate(QuatcinelleGame.WIDTH / 2, QuatcinelleGame.HEIGHT / 2);
            this.helper.drawPossibilities(this.system.possibilites);
            this.system.board.forEach((pawn) => {
                this.helper.drawPawn(pawn.pos, pawn.type);
            });
            ctx.translate(-QuatcinelleGame.WIDTH / 2, -QuatcinelleGame.HEIGHT / 2);
            return;
        }
        ctx.fillStyle = "rgba(0, 0, 0, 16)";
        ctx.fillRect(0, 0, QuatcinelleGame.WIDTH, QuatcinelleGame.HEIGHT);
        ctx.translate(QuatcinelleGame.WIDTH / 2, QuatcinelleGame.HEIGHT / 2);
        this.system.winningPawns.forEach((pawn) => {
            this.helper.drawPawn(pawn.pos, pawn.type);
        });
        ctx.translate(-QuatcinelleGame.WIDTH / 2, -QuatcinelleGame.HEIGHT / 2);
    }
}