import { AShape } from "./AShape";

export class Circle extends AShape
{
    constructor(
        public x:number = 0,
        public y:number = 0,
        public r:number = 0
    )
    {
        super();
    }

    public draw(ctx:CanvasRenderingContext2D, isFilled?:boolean)
    {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2);
        ctx.closePath();
        super.draw(ctx, isFilled);
    }
}