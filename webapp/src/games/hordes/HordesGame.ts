import { AGame } from "./../AGame";
import { HordesBanner } from "./HordesBanner";
import { HordesDrawer } from "./HordesDrawer";
import { HordesSystem } from "./HordesSystem";
import { Mouse } from "./../Mouse";
import { HorizontalSpritesheet } from "../lib/HorizontalSpritesheet";
import { Vector2 } from "../lib/Vector2";
import { Tile } from "../expanz/Tile";

export class HordesGame extends AGame<HordesSystem, HordesDrawer>
{
    private static readonly ARMAGEDDON_NB_SPRITES = 20;
    private static readonly ARMAGEDDON_ANIMATION_SPEED = 0.075;
    private static readonly ARMAGEDDON_SIZE = 150;
    private readonly banner = new HordesBanner();
    private readonly armageddonSprite = new HorizontalSpritesheet(
        this.helper.getImage("explosion")!,
        HordesGame.ARMAGEDDON_NB_SPRITES,
        HordesGame.ARMAGEDDON_ANIMATION_SPEED
    );

    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(HordesSystem, HordesDrawer, socket, data);
        this.helper.init(data.board.width, data.board.height);
        Mouse.onClick(this.onClick);
        this.system.onObjectUsedCallback = (obj:string, color:string, modifiedTiles:any[]) => {
            const pos = modifiedTiles[0]?.position;
            let tile;

            if (pos && obj === "armageddon") {
                tile = this.system.board.get(pos.x, pos.y)!;
                this.armageddonSprite.runOnce();
                this.armageddonSprite.position = tile.computePosition()!;
                this.armageddonSprite.size = {x: HordesGame.ARMAGEDDON_SIZE, y: HordesGame.ARMAGEDDON_SIZE};
                this.armageddonSprite.position.y -= (HordesGame.ARMAGEDDON_SIZE + tile.scale.y) / 2;
                this.armageddonSprite.position.x -= HordesGame.ARMAGEDDON_SIZE / 4;
                return;
            }
            this.banner.image = this.helper.getImage(obj);
            this.banner.color = color === "red" ? "rgba(0, 0, 128, 0.2)" : "rgba(128, 0, 0, 0.2)";
            this.banner.start();
        };
    }

    private onClick = () => {
        const pos = this.helper.getMousePosition();
        const objectId = this.helper.selectObject(this.system.objects);

        if (!this.system.board.get(pos.x, pos.y))
            return;
        this.system.emit("play", {position: pos, object: this.system.objects[objectId]});
        this.helper.selectObject();
    }

    protected update(d:number)
    {
        super.update(d);
        this.banner.update(d);
        this.armageddonSprite.update(d);
    }
    public draw(ctx:CanvasRenderingContext2D)
    {
        this.helper.drawBackground();
        this.helper.drawBoard(this.system.board);
        this.helper.drawUi(this.system.objects, this.system.current, this.system.next);
        this.helper.drawSelected(this.system.board, this.system.current);
        this.helper.drawObject();
        this.armageddonSprite.draw(ctx);
        this.banner.draw(ctx);
    }
}