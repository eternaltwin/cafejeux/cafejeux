import { HordesGame } from "./HordesGame";
import { Rectangle } from "./../lib/Rectangle";

export class HordesBanner extends Rectangle
{
    private static readonly HEIGHT_RATIO = 0.25;
    private static readonly IMAGE_SIZE_RATIO = 0.25;
    private static readonly DURATION = 3;
    private static readonly TRANSITION_RATIO = 10;
    private readonly HEIGHT:number;
    private readonly imageFrame:Rectangle;
    private percent = 1;
    public color = "";
    public image?:HTMLImageElement;

    constructor()
    {
        super(0, 0, HordesGame.WIDTH, HordesGame.HEIGHT * HordesBanner.HEIGHT_RATIO);
        this.y = (HordesGame.HEIGHT - this.h) / 2;
        this.HEIGHT = this.h;
        this.imageFrame = this.clone().scale(HordesBanner.IMAGE_SIZE_RATIO);
        this.imageFrame.w = this.imageFrame.h;
        this.imageFrame.x = (this.w - this.imageFrame.w) / 2;
    }

    private static easePercent(p:number)
    {
        const t = HordesBanner.TRANSITION_RATIO;
        
        if (p < 1 / t)
            return (p * t);
        if (p > 1 - 1 / t)
            return (1 - (p - (1 - 1 / t)) * t);
        return (1);
    }
    
    public start()
    {
        this.percent = 0;
    }
    public update(d:number)
    {
        this.percent += d / HordesBanner.DURATION;
    }
    public draw(ctx:CanvasRenderingContext2D)
    {
        if (this.percent >= 1)
            return;
        this.h = (this.HEIGHT * HordesBanner.easePercent(this.percent));
        ctx.fillStyle = this.color;
        ctx.strokeStyle = "rgb(224, 162, 76)";
        ctx.lineWidth = 5;
        super.draw(ctx);
        this.imageFrame.h = (this.HEIGHT * HordesBanner.easePercent(this.percent));
        this.imageFrame.h *= HordesBanner.IMAGE_SIZE_RATIO;
        this.imageFrame.drawImage(ctx, this.image);
    }
}