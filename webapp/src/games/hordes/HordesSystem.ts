import { Tile } from "../expanz/Tile";
import { AGameSystem } from "./../AGameSystem";
import { LinearMatrix } from "./../lib/LinearMatrix";
import { HordesObjects } from "./HordesObjects";
import { HordesTile, RawTile } from "./HordesTile";

export class HordesSystem extends AGameSystem
{
    private _board:LinearMatrix<HordesTile>;
    private _current:number;
    private _next:number;
    private _objects:HordesObjects;
    public readonly adversaryColor:string;
    public readonly color:string;
    public onObjectUsedCallback?:Function;
    
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        socket.on("zombie_turn", this.updateBoard);
        data.board.data = data.board.data.map((tile:RawTile) => new HordesTile(tile));
        this._board = new LinearMatrix<HordesTile>(data.board);
        this._current = data.current;
        this._next = data.next;
        this.color = data.color;
        this._objects = data.objects;
        this.adversaryColor = data.color === "red" ? "blue" : "red";
    }

    private updateBoard = (data:any) =>
    {
        let tileObj;
        
        if (!data.modifiedTiles)
            return;
        for (const tile of data.modifiedTiles) {
            tileObj = this._board.get(tile.position.x, tile.position.y);
            if (data.object === "armageddon") {
                tileObj!.quantity = -1;
                tileObj!.canBePopulated = false;
                break;
            }
            tile.color = tile.owner === data.senderId ? this.color : this.adversaryColor;
            if (!tile.owner)
                tile.color = undefined;
            this._board.set(tile.position.x, tile.position.y, new HordesTile(tile));
        }
    }

    public update(data:any)
    {
        const COLOR = data.player !== data.senderId ? this.color : this.adversaryColor;
        
        this.updateBoard(data);
        this._current = data.current;
        this._next = data.next;
        if (data.object && this.onObjectUsedCallback)
            this.onObjectUsedCallback(data.object, COLOR, data.modifiedTiles);
        if (data.player !== data.senderId)
            return;
        for (let i = this._objects.length - 1; i >= 0; i--) {
            if (this._objects[i] === data.object) {
                this._objects[i] = undefined;
                break;
            }
        }
    }
    public end(data:any)
    {
        console.log(data);
    }
    
    public get board():LinearMatrix<HordesTile>
    {
        return (this._board);
    }
    public get current():number
    {
        return (this._current);
    }
    public get next():number
    {
        return (this._next);
    }
    public get objects():HordesObjects
    {
        return (this._objects);
    }
}