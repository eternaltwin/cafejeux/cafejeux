import { ADrawer } from "./../ADrawer";
import { AmoniteGame } from "./AmoniteGame";
import { AmoniteBoardType } from "./AmoniteBoardType";
import { Mouse } from "./../Mouse";
import { Rectangle } from "./../lib/Rectangle";
import { Vector2 } from "./../lib/Vector2";

export class AmoniteDrawer extends ADrawer
{
    private static readonly MIN_HEIGHT = 5;
    private static readonly MAX_HEIGHT = 9;
    private static readonly NB_COLUMNS = ~~((AmoniteDrawer.MAX_HEIGHT - AmoniteDrawer.MIN_HEIGHT) * 2) + 1;
    private static readonly PAWN_SIZE_RATIO = 0.7;
    private static readonly HEXAGON_SLOPE_RATIO = (1 / 3);
    private static readonly MARGIN = {x: 23, y: 10};
    private readonly tileSize = {
        x: (AmoniteGame.WIDTH - (AmoniteDrawer.MARGIN.x * 2)) / AmoniteDrawer.NB_COLUMNS,
        y: (AmoniteGame.HEIGHT - (AmoniteDrawer.MARGIN.y * 2)) / AmoniteDrawer.MAX_HEIGHT
    };
    private readonly PAWN_SIZE:number;
    private readonly tileRect = new Rectangle();
    private mousePosition:Vector2 = {x: -1, y: -1};
    public color?:string;

    constructor(ctx:CanvasRenderingContext2D)
    {
        super(ctx);
        this.tileRect.w = this.tileSize.x + this.tileSize.x * AmoniteDrawer.HEXAGON_SLOPE_RATIO;
        this.tileRect.h = this.tileSize.y;
        this.PAWN_SIZE = this.tileRect.w * AmoniteDrawer.PAWN_SIZE_RATIO;
    }
    
    private convertRelativeToAbsolutePosition(relative:Vector2, colsize:number):Vector2
    {
        const Y = this.tileSize.y * ((AmoniteDrawer.MAX_HEIGHT - colsize) / 2);
        const output = {
            x: AmoniteDrawer.MARGIN.x + this.tileSize.x * relative.x,
            y: AmoniteDrawer.MARGIN.y + Y + this.tileSize.y * relative.y
        };

        output.x *= (1 - (AmoniteDrawer.HEXAGON_SLOPE_RATIO / AmoniteDrawer.NB_COLUMNS));
        return (output);
    }
    private drawHexagonFromRectangle(rect:Rectangle)
    {
        const SLOPE = rect.w * AmoniteDrawer.HEXAGON_SLOPE_RATIO;
        
        this.ctx.fillStyle = `rgba(0, 255, 255, ${0.1 + Math.abs(Math.cos(this.ratio)) * 0.2})`;
        this.ctx.beginPath();
        this.ctx.moveTo(rect.x + SLOPE, rect.y);
        this.ctx.lineTo(rect.x, rect.y + rect.h / 2);
        this.ctx.lineTo(rect.x + SLOPE, rect.y + rect.h);
        this.ctx.lineTo(rect.x + rect.w - SLOPE, rect.y + rect.h);
        this.ctx.lineTo(rect.x + rect.w, rect.y + rect.h / 2);
        this.ctx.lineTo(rect.x + rect.w - SLOPE, rect.y);
        this.ctx.closePath();
        this.ctx.fill();
    }
    private drawColumn = (col:Array<string|undefined>, n:number) =>
    {
        const INFLATED_SIZE = this.PAWN_SIZE * (1 + Math.abs(Math.cos(this.ratio)) * 0.1);
        let pos:Vector2;
        let currentSize:number;

        for (let y = col.length - 1; y >= 0; y--) {
            pos = this.convertRelativeToAbsolutePosition({x: n, y}, col.length);
            this.tileRect.x = pos.x;
            this.tileRect.y = pos.y;
            this.ctx.drawImage(this.getImage("hexagon")!, 
                this.tileRect.x, this.tileRect.y, 
                this.tileRect.w + 1, this.tileRect.h + 1
            );
            if (col[y]) {
                currentSize = (col[y] === this.color) ? INFLATED_SIZE : this.PAWN_SIZE;
                this.ctx.drawImage(this.getImage(col[y]!)!,
                    this.tileRect.x + (this.tileRect.w - currentSize) / 2,
                    this.tileRect.y + (this.tileRect.h - currentSize) / 2,
                    currentSize, currentSize
                );
            }
            if (this.tileRect.isPointInside(Mouse.x, Mouse.y)) {
                this.mousePosition = {x: n, y};
                this.drawHexagonFromRectangle(this.tileRect);
            }
        }
    }
    
    public preload()
    {
        const BASE_URL = `${process.env.PUBLIC_URL}/assets/amonite`;
        
        this.addImage("background", `${BASE_URL}/background.png`);
        this.addImage("hexagon", `${BASE_URL}/hexagon.png`);
        this.addImage("red", `${BASE_URL}/red.png`);
        this.addImage("yellow", `${BASE_URL}/yellow.png`);
    }
    public drawBackground()
    {
        this.ctx.drawImage(this.getImage("background")!, 0, 0, AmoniteGame.WIDTH, AmoniteGame.HEIGHT);
    }
    public drawBoard(board:AmoniteBoardType)
    {
        for (let i = board.length - 1; i >= 0; i--) {
            board.forEach(this.drawColumn);
        }
    }
    public drawSelected(pos:Vector2, colsize:number|undefined)
    {
        let absolute:Vector2;
        
        if (!colsize)
            return;
        absolute = this.convertRelativeToAbsolutePosition(pos, colsize);
        this.tileRect.x = absolute.x;
        this.tileRect.y = absolute.y;
        this.drawHexagonFromRectangle(this.tileRect);
    }
    public drawPossibilities(board:AmoniteBoardType, possibilities:Vector2[])
    {
        possibilities.forEach((pos) => {
            this.drawSelected(pos, board[pos.x].length);
        });
    }
    public getMousePosition()
    {
        return (this.mousePosition);
    }
}