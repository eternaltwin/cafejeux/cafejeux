import { MagmaxPawn } from "./MagmaxPawn";

export interface MagmaxTile
{
    hasBomb?:boolean;
    hasRock?:boolean;
    pawn:MagmaxPawn;
}