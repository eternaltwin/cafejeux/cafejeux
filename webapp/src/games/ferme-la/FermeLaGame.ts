import { AGame } from "./../AGame";
import { FermeLaDrawer } from "./FermeLaDrawer";
import { FermeLaSystem } from "./FermeLaSystem";
import { Mouse } from "./../Mouse";

export class FermeLaGame extends AGame<FermeLaSystem, FermeLaDrawer>
{
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(FermeLaSystem, FermeLaDrawer, socket, data);
        this.helper.init(this.system.width, this.system.height);
        Mouse.onClick(() => {
            const intersections = this.helper.getMouseIntersections();
            
            if (intersections.length === 0)
                return;
            this.system.emit("play", {
                start: intersections[0],
                end: intersections[1]
            });
        });
    }

    public draw(ctx:CanvasRenderingContext2D)
    {
        ctx.lineCap = "round";
        ctx.drawImage(
            this.helper.getImage("background")!,
            0, 0, FermeLaGame.WIDTH, FermeLaGame.HEIGHT
        );
        this.system.grid.forEach((tile, x, y) => {
            if (tile)
                this.helper.drawTile(tile, {x, y});
        });
        this.helper.drawSelectedIntersection();
        this.system.links.forEach((v) => {
            this.helper.drawIntersection(v[0], v[1]);
        });
        ctx.drawImage(
            this.helper.getImage("foreground")!,
            0, 0, FermeLaGame.WIDTH, FermeLaGame.HEIGHT
        );
    }
}