import { AGame } from "./../AGame";
import { Mouse } from "./../Mouse";
import { TrifusDrawer } from "./TrifusDrawer";
import { TrifusSystem } from "./TrifusSystem";

export class TrifusGame extends AGame<TrifusSystem, TrifusDrawer>
{
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(TrifusSystem, TrifusDrawer, socket, data);
        this.helper.init(this.system.getWidth(), this.system.getHeight());
        Mouse.onClick(() => {
            const isUpdated = this.helper.updateSelectedCard();
            
            if (!isUpdated && this.helper.selectedIndex !== -1) {
                this.system.emit("play", {
                    position: this.helper.getMousePosition(),
                    card: this.system.deck[this.helper.selectedIndex].toData()
                });
                this.helper.resetSelectedCard();
            }
        });
    }

    public draw(ctx:CanvasRenderingContext2D)
    {
        let card;

        ctx.font = "13px Verdana bold";
        this.helper.drawBackground(this.system.color);
        this.helper.drawDeck(this.system.deck);
        for (let x = 0, w = this.system.getWidth(); x < w; x++) {
            for (let y = 0, h = this.system.getHeight(); y < h; y++) {
                card = this.system.getCard(x, y);
                if (card)
                    this.helper.drawCardOnBoard(card, {x: x, y: y});
            }
        }
    }
}