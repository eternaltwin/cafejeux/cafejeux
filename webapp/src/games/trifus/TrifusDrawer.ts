import { ADrawer } from "./../ADrawer";
import { Mouse } from "./../Mouse";
import { Rectangle } from "./../lib/Rectangle";
import { TrifusCard } from "./TrifusCard";
import { TrifusColor } from "./TrifusColor";
import { TrifusGame } from "./TrifusGame";
import { TrifusType } from "./TrifusType";
import { Vector2 } from "./../lib/Vector2";

export class TrifusDrawer extends ADrawer
{
    public static readonly OVERLAY_HEIGHT_RATIO = 0.245;
    public static readonly PADDING_RATIO = 1 / 30;

    private readonly overlayBox = new Rectangle();
    public readonly scale:Vector2 = {x: 0, y: 0};
    public readonly padding:Vector2 = {
        x: TrifusGame.WIDTH * TrifusDrawer.PADDING_RATIO,
        y: TrifusGame.HEIGHT * TrifusDrawer.PADDING_RATIO
    };
    private hoverIndex:number = -1;
    private _selectedIndex:number = -1;

    private drawCard(card:TrifusCard, pos:Vector2)
    {
        TrifusCard.image = this.getImage(card.type);
        card.x = pos.x;
        card.y = pos.y;
        card.draw(this.ctx);
    }

    public preload():void
    {
        const BASE = `${process.env.PUBLIC_URL}/assets/trifus`;
        const TYPES = Object.values(TrifusType);

        this.addImage("bg", `${BASE}/bg.png`);
        this.addImage("blue_overlay", `${BASE}/blue_overlay.png`);
        for (let i = TYPES.length - 1; i >= 0; i--) {
            this.addImage(TYPES[i], `${BASE}/${TYPES[i]}.png`);
        }
    }
    public update(d:number)
    {

    }
    public getMousePosition():Vector2
    {
        return ({
            x: ~~((Mouse.x - this.padding.x) / this.scale.x),
            y: ~~((Mouse.y - this.padding.y) / this.scale.y)
        });
    }
    public init(w:number, h:number)
    {
        const SIZE:Vector2 = {
            x: TrifusGame.WIDTH - this.padding.x * 2,
            y: TrifusGame.HEIGHT * (1 - TrifusDrawer.OVERLAY_HEIGHT_RATIO) - this.padding.y
        };

        this.scale.x = SIZE.x / w;
        this.scale.y = SIZE.y / h;
        TrifusCard.init(this.scale);
        this.overlayBox.x = TrifusCard.size.x / 2;
        this.overlayBox.h = TrifusGame.HEIGHT * TrifusDrawer.OVERLAY_HEIGHT_RATIO;
        this.overlayBox.y = TrifusGame.HEIGHT - this.overlayBox.h + this.padding.y * 2;
        this.overlayBox.w = TrifusGame.WIDTH - this.overlayBox.x * 2 - this.padding.x;
    }
    public resetSelectedCard()
    {
        this._selectedIndex = -1;
    }
    /**
     * Returns true if the selected cas has been changed, false otherwise.
     */
    public updateSelectedCard():boolean
    {
        if (this.hoverIndex !== -1)
            this._selectedIndex = this.hoverIndex;
        return (this.hoverIndex !== -1);
    }
    public drawBackground(color:TrifusColor)
    {
        this.ctx.drawImage(this.getImage("bg")!, 0, 0, TrifusGame.WIDTH, TrifusGame.HEIGHT);
        if (color === "blue") {
            this.ctx.drawImage(this.getImage("blue_overlay")!,
                0, TrifusGame.HEIGHT - this.overlayBox.h,
                TrifusGame.WIDTH, this.overlayBox.h
            );
        }
    }
    public drawCardOnBoard(card:TrifusCard, pos:Vector2)
    {
        pos.x = pos.x * this.scale.x + this.padding.x;
        pos.y = pos.y * this.scale.y + this.padding.y;
        pos.x += (this.scale.x - TrifusCard.size.x) / 2;
        pos.y += (this.scale.y - TrifusCard.size.y) / 2;
        this.drawCard(card, pos);
    }
    public drawDeck(cards:TrifusCard[])
    {
        const LEN = cards.length;
        let pos:Vector2;

        this.hoverIndex = -1;
        for (let i = LEN - 1; i >= 0; i--) {
            if (cards[i].isPointInside(Mouse.x, Mouse.y)) {
                this.hoverIndex = i;
                break;
            }
        }
        for (let i = 0; i < LEN; i++) {
            pos = {
                x: this.overlayBox.x + (this.overlayBox.w / LEN) * i,
                y: this.overlayBox.y
            };
            cards[i].isSelected = this.hoverIndex === i || this._selectedIndex === i;
            this.drawCard(cards[i], pos);
        }
    }

    private get isMouseOnDeck():boolean
    {
        return (Mouse.y >= this.overlayBox.y && Mouse.y <= TrifusGame.HEIGHT);
    }
    public get selectedIndex():number
    {
        return (this._selectedIndex);
    }
}