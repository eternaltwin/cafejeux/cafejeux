import React from "react";
import { useHistory } from "react-router-dom";
import BackgroundImageContainer from "./generics/BackgroundImageContainer";
import NavbarIcon from "./components/NavbarIcon";
import StylesDictionary from "./generics/StylesDictionary";
import theme from "./Theme";

// Temporary arrays
const buttons = [
    {text: "Jouer Au Bar", icon: "play", to: "/"},
    {text: "Classement", icon: "ranking", to: "/"},
    {text: "Messages", icon: "mail", to: "/"},
    {text: "Boutique", icon: "shop", to: "/"},
    {text: "+ de sucres", icon: "bank", to: "/"}
];

const subMenuButtons = [
    {text: "Tables de jeu"},
    {text: "Nouveautés"},
    {text: "Forums"},
    {text: "Ma page"},
    {text: "Aide"},
    {text: "Deconnexion"}
];

export default function Navbar()
{
    const history = useHistory();
    
    const generateIconMenu = () => {
        return (buttons.map((button, index) => {
            return (
                <NavbarIcon
                    key={index}
                    url={button.icon + ".gif"}
                    text={button.text}
                    onClick={() => history.push(button.to)}
                    isMain={index === 0}
                />
            );
        }));
    }

    const generateSubMenu = () => {
        return (subMenuButtons.map((button, index) => {
            return (
                <li key={index} className="drop-caps link">{button.text}</li>
            );
        }));
    }
    
    return (
        <BackgroundImageContainer
            url="menuBg.gif"
            style={styles.container}
            noCover
        >
            <div style={styles.iconMenu}>
                {generateIconMenu()}
            </div>
            <hr style={styles.separator} />
            <ul style={styles.menu}>
                {generateSubMenu()}
            </ul>
            <hr style={styles.separator} />

        </BackgroundImageContainer>
    );
}

const styles:StylesDictionary = {
    container: {
        height: "100%",
        backgroundColor: theme.primary.dark,
        backgroundPosition: "center 80px",
        backgroundRepeat: "no-repeat",
        textTransform: "uppercase"
    },
    iconMenu: {
        display: "flex",
        justifyContent: "space-around",
        flexWrap: "wrap"
    },
    menu: {
        marginLeft: 20,
        fontSize: "0.8em"
    },
    separator: {
        margin: "10px auto",
        width: "75%",
        opacity: 0.5,
        border: `1px ${theme.primary.main} solid`
    }
};