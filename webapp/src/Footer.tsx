import React from "react";
import BackgroundImageContainer from "./generics/BackgroundImageContainer";
import StylesDictionary from "./generics/StylesDictionary";

export default function Footer()
{
    return (
        <BackgroundImageContainer
            url="content_footer.gif"
            style={styles.footer}
            noCover
        >

        </BackgroundImageContainer>
    );
}

const styles:StylesDictionary = {
    footer: {
        width: 1000,
        height: 350,
        backgroundSize: "contain",
        backgroundRepeat: "no-repeat"
    }
};