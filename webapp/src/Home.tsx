import React from "react";
import GameBox from "./components/GameBox";
import StylesDictionary from "./generics/StylesDictionary";
import * as gameList from "./json/games.json";

export default function Home()
{
    const generateGame = () => {
        return (gameList.data.map((game) => {
            return (
                <GameBox
                    key={game.name}
                    name={game.name}
                    img={game.img}
                    diffucilty={game.difficulty}
                />
            );
        }));
    };
    
    return (
        <div>
            <p style={styles.text}>
                Cliquez sur la boite du jeu qui vous intéresse.<br/>
                <em style={styles.em}>Chaque partie jouée au bar compte pour le classement des jeux.</em>
            </p>
            <div style={styles.gameContainer}>
                {generateGame()}
            </div>
        </div>
    );
}

const styles:StylesDictionary = {
    text: {
        marginLeft: 50,
        fontSize: "1em"
    },
    em: {
        fontSize: "0.9em"
    },
    gameContainer: {
        marginLeft: 20,
        display: "flex",
        flexWrap: "wrap"
    }
}