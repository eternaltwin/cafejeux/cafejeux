import { CSSProperties } from "react";

export default interface StylesDictionary
{
    [Key:string]:CSSProperties;
}