import React from "react";
import theme from "../Theme";

interface BicoloredTextProps
{
    text:string;
    style?:React.CSSProperties;
}

export default function BicoloredText(props:BicoloredTextProps)
{
    const splittedText = props.text.split(" ");
    const firstHalf = splittedText.splice(0, splittedText.length / 2).join(" ");
    const secondHalf = splittedText.join(" ");
    
    return (
        <p style={props.style}>
            <span style={{color: theme.primary.light}}>{firstHalf + " "}</span>
            <span>{secondHalf}</span>
        </p>
    );
}