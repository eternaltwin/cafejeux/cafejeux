import { ExpanzTypes } from "./ExpanzTypes";
import { LinearMatrix, Vector2 } from "./../../Utils";

export class ExpanzBoard extends LinearMatrix<ExpanzTypes.Color>
{
	public takenColors:ExpanzTypes.Color[] = [];

	constructor(size:number)
	{
		super(size);
		for (let x = 0; x < size; x++) {
			for (let y = 0; y < size; y++) {
				this.set(x, y, ~~(Math.random() * ExpanzTypes.Color.COUNT));
			}
		}
	}
	/**
	 * Returns true if the targetted tile has an access to expand the terriroty of its player, false otherwise.
	 * A tile can expand the territory if one of its border is linked to a color which isn't in the colors array.
	 *
	 * @params tile:Vector2 => The position of the tile to check.
	 * @params colors:ExpanzTypes.Color[] => The colors where the tile can't be used for an expansion.
	 * @returs A boolean indicates whether or not the tile can be used for expand.
	 */
	private isTileOpenedToExpand(tile:Vector2, colors:(ExpanzTypes.Color|undefined)[]):boolean
	{
		const dir = [
			{x: -1, y: 0},
			{x: 1, y: 0},
			{x: 0, y: -1},
			{x: 0, y: 1}
		];
		let tileColor:ExpanzTypes.Color|undefined;
		
		for (let i = dir.length - 1; i >= 0; i--) {
			tileColor = this.get(tile.x - dir[i].x, tile.y - dir[i].y);
			if (tileColor == undefined)
				continue;
			if (colors.find((c) => tileColor == c) == undefined) {
				return (true);
			}
		}
		return (false);
	}
	/**
	 * Check that the player's move is on a free color.
	 * The move is considerated as valid if:
	 * - the choosen color is not taken by another player
	 * - the choosen tile does not overlap the territory of another player.
	 *
	 * @params pos:Vector2 The position choosen by the player.
	 * @returns A boolean indicates whether or not the color is already taken.
	 */
	public isColorAlreadyTaken(pos:Vector2):boolean
	{
		const colors = this.takenColors.filter((c) => (this.get(pos) == c));
		
		if (colors.length > 0)
			return (false);
		if (this.get(pos) == ExpanzTypes.Color.TAKEN)
			return (false);
		return (true);
	}
	/**
	 * Set every tile of a given territory to the color passed as parameter
	 *
	 * @params territory:Vector2[] => The territory to paint.
	 * @params color:ExpanzTypes.Color => The color to be painted.
	 */
	public paint(territory:Vector2[], color:ExpanzTypes.Color):void
	{
		territory.forEach((v) => {
			this.set(v.x, v.y, color);
		});
	}
	/**
	 * Returns true if the territory is locked, and player can't play, false otherwise.
	 * A territory is considerated as locked if:
	 * - all tiles around him are the same color as an already taken one.taken
	 * - its territory is surrounded by others territories.
	 *
	 * @params territory:Vector2[] => The territory to check.
	 * @returs A boolean indicates whether or not the territory is locked.
	 */
	public isTerritoryLocked(territory:Vector2[]):boolean
	{
		const colors = [ExpanzTypes.Color.TAKEN, undefined].concat(this.takenColors);
		if (territory.length == 0)
			return (false);
		for (let i = territory.length - 1; i >= 0; i--) {
			if (this.isTileOpenedToExpand(territory[i], colors)) {
				return (false);
			}
		}
		return (true);
	}
	/**
	 * This function will search every tile which is the same color of the current, and linked to it.
	 * Every tile considerated as valid by the method will be paint with the "TAKEN" status.
	 *
	 * @params pos:{x:number, y:number} => The tile from where the algorithm will start its research.
	 */
	public spread(pos:Vector2, color?:ExpanzTypes.Color):Vector2[]
	{
		let output:Vector2[] = [];
		let cur = this.get(pos);

		if (color == undefined) {
			color = cur;
		}
		if (cur == undefined || cur != color || cur == ExpanzTypes.Color.TAKEN)
			return ([]);
		this.set(pos.x, pos.y, ExpanzTypes.Color.TAKEN);
		output = output.concat(this.spread({x: pos.x + 1, y: pos.y}, color));
		output = output.concat(this.spread({x: pos.x - 1, y: pos.y}, color));
		output = output.concat(this.spread({x: pos.x, y: pos.y + 1}, color));
		output = output.concat(this.spread({x: pos.x, y: pos.y - 1}, color));
		return (output.concat(pos));
	}
}