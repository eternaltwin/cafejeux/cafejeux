import { IReceivedPacket, ISendPacket, PlayerId } from "etwin-socket-server";
import { ExpanzTypes } from "./ExpanzTypes";
import { LinearMatrix, Vector2 } from "./../../Utils";

export namespace ExpanzPackets
{
	export interface PlayedTurn extends IReceivedPacket
	{
		position:Vector2;
	}
	export interface UpdateBoard extends ISendPacket
	{
		playerId:PlayerId;
		color:ExpanzTypes.Color;
		territory:Vector2[];
	}
	export interface GetBoard extends ISendPacket
	{
		board:LinearMatrix<ExpanzTypes.Color>;
	}
	export interface End extends ISendPacket
	{
		hasWon:boolean;
	}
}