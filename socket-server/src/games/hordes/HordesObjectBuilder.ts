import { HordesBoard } from "./HordesBoard";
import * as Objects from "./HordesObjects";
import { HordesPlayer } from "./HordesPlayer";

export class HordesObjectBuilder
{
    private static readonly OBJECTS = [
        Objects.Armageddon, Objects.BigCuteCat,
        Objects.CallHelp, Objects.MachineGun,
        Objects.RustyGun, Objects.SmallHanging,
        Objects.ViciousTrap, Objects.WaterRation
    ];

    public static generate(board:HordesBoard, player:HordesPlayer, nbObjects = 4):Objects.AHordesObject[]
    {
        const output:Objects.AHordesObject[] = [];
        let rnd:number;

        for (let i = nbObjects; i > 0; i--) {
            rnd = ~~(Math.random() * HordesObjectBuilder.OBJECTS.length);
            output.push(new HordesObjectBuilder.OBJECTS[rnd](board, player));
        }
        return (output);
    }
}