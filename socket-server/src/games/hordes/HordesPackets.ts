import { ISendPacket, IReceivedPacket, PlayerId } from "etwin-socket-server";
import { HordesBoard } from "./HordesBoard";
import { HordesTypes } from "./HordesTypes";
import { Vector2 } from "./../../Utils";

export module HordesPackets
{
    export interface Start extends ISendPacket
    {
        board:HordesBoard;
        current:number;
        next:number;
        color:string;
        objects:string[];
    }
    export interface Update extends ISendPacket
    {
        modifiedTiles:HordesTypes.ModifiedTile[];
        player:PlayerId;
        current:number;
        next:number;
        object:string|undefined;
    }
    export interface ZombieTurn extends ISendPacket
    {
        modifiedTiles:HordesTypes.ModifiedTile[];
        currentTurn:number;
    }
    export interface End extends ISendPacket
    {
        score:number;
        hasWon:boolean;
    }
    export interface Play extends IReceivedPacket
    {
        position:Vector2;
        object:string|undefined;
    }
}