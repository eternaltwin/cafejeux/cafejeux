import { CafejeuxPlayer } from "./../../CafejeuxPlayer";
import { TrifusTypes } from "./TrifusTypes";

export class TrifusPlayer extends CafejeuxPlayer
{
	private _deck:TrifusTypes.Card[] = [];
    private _color?:TrifusTypes.Color;
    public score = 0;

    private updateDeckColor()
    {
        for (let i = this._deck.length - 1; i >= 0; i--) {
            this._deck[i].owner = this._color;
        }
    }
    
    /**
     * Check that the card passed as parameter is inside the player's deck.
     *
     * @params card:TrifusTypes.Card = The card object to be tested.
     * @returns The picked card if it exists, undefined otherwise.
     */
    public pickCard(card?:TrifusTypes.Card):TrifusTypes.Card|undefined
    {
        let cur:TrifusTypes.Card;
        
        if (!card)
            return (undefined);
        for (let i = this._deck.length - 1; i >= 0; i--) {
            cur = this._deck[i];
            if (cur.element === card.element || cur.type === card.type) {
                if (cur.strength === card.strength)
                    return (this._deck.splice(i, 1)[0]);
            }
        }
        return (undefined);
    }
    public get deck():TrifusTypes.Card[]
    {
        return (this._deck);
    }
    public get color():TrifusTypes.Color
    {
        return (this._color!);
    }
	public set deck(d:TrifusTypes.Card[])
	{
		if (this._deck.length <= 0) {
			this._deck = d;
            this.updateDeckColor();
        }
	}
    public set color(color:TrifusTypes.Color)
    {
        if (!this._color) {
            this._color = color;
            this.updateDeckColor();
        }
    }
}