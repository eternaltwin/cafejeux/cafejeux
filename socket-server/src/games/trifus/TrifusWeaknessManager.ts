import { TrifusTypes } from "./TrifusTypes";

export class TrifusWeaknessManager
{
    private readonly typeWeaknesses:number[];
    
    constructor()
    {
        this.typeWeaknesses = [
            TrifusWeaknessManager.collide(TrifusTypes.Element.FIRE, TrifusTypes.Element.WATER),
            TrifusWeaknessManager.collide(TrifusTypes.Element.WATER, TrifusTypes.Element.WOOD),
            TrifusWeaknessManager.collide(TrifusTypes.Element.WOOD, TrifusTypes.Element.FIRE),
        ];
    }

    private static collide(t1:TrifusTypes.Element, t2:TrifusTypes.Element)
    {
        return ((t1 * t1) - (t2 * t2));
    }
    /**
     * Check if the type given as first argument is weak to the second type.
     *
     * @params defenser:TrifusTypes.Element = The type to be tested.
     * @params attacker:TrifusTypes.Element = The type that will test the defensive type.
     * @returns True if the first type is weaker, false athoerwise.
     */
    private isTypeWeak(defenser:TrifusTypes.Element, attacker:TrifusTypes.Element):boolean
    {
        const c = TrifusWeaknessManager.collide(defenser, attacker);
        
        for (let i = this.typeWeaknesses.length - 1; i >= 0; i--) {
            if (c === this.typeWeaknesses[i])
                return (true);
        }
        return (false);
    }

    /**
     * Check if the card given as first argument is weaker than the second card.
     *
     * @params defenser:TrifusTypes.Card = The card to be tested.
     * @params attacker:TrifusTypes.Card = The card that will test the defensive card.
     * @returns True if the first card is weaker, false athoerwise.
     */
    public isCardWeaker(defenser:TrifusTypes.Card, attacker:TrifusTypes.Card):boolean
    {
        if (this.isTypeWeak(defenser.element, attacker.element))
            return (true);
        if (defenser.element === attacker.element) {
            return (defenser.strength < attacker.strength);
        }
        return (false);
    }
}