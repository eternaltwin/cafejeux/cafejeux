import { PlayerId } from "etwin-socket-server";
import { MagmaxTypes } from "./MagmaxTypes";

export class MagmaxPawn
{
    private static currentInstance = 0;
    public seeBombs = false;
    public isProtected = false;

    constructor(
        public readonly owner:PlayerId,
        public orientation:MagmaxTypes.Orientation,
        public readonly ID:number = MagmaxPawn.currentInstance
    )
    {
        MagmaxPawn.currentInstance++;
    }

    public getData(hidden:boolean):MagmaxPawn
    {
        const pawn = new MagmaxPawn(this.owner, this.orientation, this.ID);
        
        pawn.seeBombs = !hidden && this.seeBombs;
        pawn.isProtected = !hidden && this.isProtected;
        return (pawn);
    }
}