import { Vector2 } from "./../../Utils";

export module FermeLaTypes
{
    export interface Tile<T> extends Vector2<T>
    {
        animal?:FermeLaTypes.Animals;
    }
    export enum Animals
    {
        DEFAULT = "none",
        SHEEP = "sheep",
        COW = "cow"
    }
}