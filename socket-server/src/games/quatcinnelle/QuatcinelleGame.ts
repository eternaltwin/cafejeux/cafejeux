import { Vector2 } from "./../../Utils";
import { ACafejeuxGame } from "./../../ACafejeuxGame"
import { CafejeuxPlayer } from "./../../CafejeuxPlayer"; 
import { QuatcinelleBoard } from "./QuatcinelleBoard";
import { QuatcinellePackets } from "./QuatcinellePacket";
import { IReceivedPacket } from "etwin-socket-server";

export class QuatcinelleGame extends ACafejeuxGame<CafejeuxPlayer>
{
    public static readonly NB_PAWNS_FOR_WINNING = 4;
    private readonly board = new QuatcinelleBoard();
    private winningAlignment?:Vector2[];

    constructor(opts:any)
    {
        super(opts);
        this.registerReceiveEvent("play", this.playTurn);
    }
    
    private playTurn = (data:QuatcinellePackets.Play, emitter:CafejeuxPlayer) =>
    {
        const pos = data.position;
            
        if (!this.checkTurnIntegrity(emitter.ID) || !pos)
            return;
        if (!this.board.isMoveValid(pos))
            return;
        this.board.setPawn(pos.x, pos.y, emitter.ID);
        this.broadcast<QuatcinellePackets.Play>("update", {
            position: pos,
            owner: emitter.ID
        });
        this.winningAlignment = this.board.getWinningAlignment(pos);
        if (this.winningAlignment!.length >= QuatcinelleGame.NB_PAWNS_FOR_WINNING) {
            this.stop();
            return;
        }
        this.changeTurn();
    }

    protected getDataOnReconnection():IReceivedPacket
    {
        return (<QuatcinellePackets.Reconnect>{
            pawns: this.board.getAll()
        });
    }
    
    public run()
    {
        this.broadcast("start", {});
    }
    public close()
    {
        this.apply((p:CafejeuxPlayer) => {
            p.send<QuatcinellePackets.End>("end", {
                pawns: this.winningAlignment!,
                hasWon: p.ID == this.currentPlayer.ID
            });
        });
    }
}