import dotenv from "dotenv";
import { SocketServer } from "etwin-socket-server";
import { CafejeuxPlayer } from "./CafejeuxPlayer";
import { ACafejeuxGame } from "./ACafejeuxGame";
import { AmoniteGame } from "./games/amonite/AmoniteGame";
import { ExpanzGame } from "./games/expanz/ExpanzGame";
import { ExpanzPlayer } from "./games/expanz/ExpanzPlayer";
import { FermeLaGame } from "./games/ferme-la/FermeLaGame";
import { FermeLaPlayer } from "./games/ferme-la/FermeLaPlayer";
import { HordesGame } from "./games/hordes/HordesGame";
import { HordesPlayer } from "./games/hordes/HordesPlayer";
import { MagmaxGame } from "./games/magmax/MagmaxGame";
import { MagmaxPlayer } from "./games/magmax/MagmaxPlayer";
import { QuatcinelleGame } from "./games/quatcinnelle/QuatcinelleGame";
import { TrifusGame } from "./games/trifus/TrifusGame";
import { TrifusPlayer } from "./games/trifus/TrifusPlayer";

async function main():Promise<void>
{
	const ss = new SocketServer<ACafejeuxGame<any>, CafejeuxPlayer>();

	dotenv.config();
	ss.addInstanciator("amonite", AmoniteGame, CafejeuxPlayer);
	ss.addInstanciator("expanz", ExpanzGame, ExpanzPlayer);
    ss.addInstanciator("ferme-la", FermeLaGame, FermeLaPlayer);
    ss.addInstanciator("hordes", HordesGame, HordesPlayer);
    ss.addInstanciator("magmax", MagmaxGame, MagmaxPlayer);
	ss.addInstanciator("quatcinelle", QuatcinelleGame, CafejeuxPlayer);
	ss.addInstanciator("trifus", TrifusGame, TrifusPlayer);
	ss.createServer(SocketServer.Type.IO, {
		port: 8888,
		cors: {
			origin: "http://localhost:3000",
			methods: ["GET", "POST"],
			credentials: true
		}
	});
	ss.run();
}

main().catch((err:Error) => {
	console.log(err.stack);
	process.exit(1);
});
