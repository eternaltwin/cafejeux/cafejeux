# Ferme La Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.

Content:
* animals: Array<FermeLaTypes.Tile>
* links: Array<Vector2[]>
* width: number
* height: number

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns the link that was created, the squares formed by the current player, its score and its id.

Content:
* link: Vector2[];
* score: number;
* squares: Vector2[];
* owner: PlayerId;

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the next turn occurs, always after the **update** event.\

Content:
* current:PlayerId
* remainingTime:number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\

Content:
* hasWon: boolean

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Send the position where the player wish to play.\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The created link is out of the board.*
* *The created link overlap another existing link.*

Content:
* start: {x: number, y: number}
* end: {x: number, y: number}

## Objects

### PlayerId

PlayerId is a simple string.\

### FermeLaTypes.Tile

It is a vector with a string to indicate if there is an animal or not.\

Content:
* x: number
* y: number
* type: "none"|"cow"|"chicken"