# Hordes Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.
It will send the player's objects, the whole map, and the number of pawns that the player can play during his/her turn and his/her next turn.

Content:
* board:LinearMatrix<HordesTypes.Tile|undefined>;
* current:number;
* next:number;
* color:string;
* objects:string[];

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns the list of every modified tile during the player's move, the id of the player who played this move, and an eventual object if an object has been played.\
It also returns the amount of pawns that the player can play.

Content:
* modifiedTiles:HordesTypes.ModifiedTile[];
* player:PlayerId;
* current:number;
* next:number;
* object:string|undefined;

### Update board

Event name: **zombie_turn**

Description:\
This event is triggered from the third turn, each time that all player played their turn. During this turn, a bot will place a group of zombies randomly. This may have an impact on the player's strategy.\
It also send the current turn to the player.\
*(Aparte: This may be a wrong architecture), in a future version the currentTurn variable may be moved to the update package.)*

Content:
* modifiedTiles:HordesTypes.ModifiedTile[];
* currentTurn:number;

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the next turn occurs, always after the **update** event.\

Content:
* current:PlayerId
* remainingTime:number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\

Content:
* score:number;
* hasWon:boolean;

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Send the position where the player wish to play.\
And object name may also be sent.\
! In this case, you must also send a position, including when the object does not require it (Any position should work in this case).\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The position has already been taken.*
* *The object can't be used on the tile where it is used.*

Content:
* position:Vector2;
* object:string|undefined;

## Objects

### PlayerId

PlayerId is a simple string.\

### LinearMatrux<HordesTypes.Tile>

A linear matrix is a complex object, it is a multidimentional array optimized to fit in a single-dimention array.\
It is possible to reconstitute a bidimentional array by splitting the array in subarray of length "*width*", or access a tile at (x, y) with the following operation: ```y * width + x```.

Content:
* data: Array<HordesTypes.Tile>
* width: number
* height: number

### HordesTypes.Tile

Content:
* quantity:number;
* owner?:PlayerId;

### HordesTypes.ModifiedTile

Content:
* quantity:number;
* owner?:PlayerId;
* position:Vector2;

### HordesTypes.Object

A card is a string restricted and describing a command.
Every object has a full random probability to appear in the player's hand.
Here are all the possible values:
```"armageddon"|"big_cute_cat"|"call_help"|"machine_gun"|"rusty_gun"|"small_hanging"|"vicious_trap"|"water_ration"```

* **armageddon (oponnent & skip)**: Destroys the tile and everything on it, the tile is not anymore usable. 
* **big_cute_cat (oponnent & skip)**: Can kill between 2 & 4 pawns of the targetted group.
* **call_help (ally)**: Add 1 pawn to the targetted group.
* **machine_gun (adversary & skip)**: Shot 9 times on the targetted group but also on tiles around it (randomly).
* **rusty_gun (adversary)**: Kill 1 pawn of the targetted group.
* **small_hanging (*)**: Destroy the current player's group and replace it by its next group.
* **vicious_trap (*)**: Must be placed on a free time. When a player fall on it, if a player places a group of pawn on it, 3 of them will be killed.
* **water_ration (*)**: The player can play 2x during this turn.

*"oponnent" means that the tile can only be used on an oponnent, "ally" only on your own pawns, * on any pawn.*\

*"skip" means that the player's turn will be skipped if the object is used.*