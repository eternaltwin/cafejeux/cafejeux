# Quatcinelle Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.

Content:
*(nothing)*

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns the id of the player who played, and the position where the pawn was placed.

Content:
* owner: PlayerId
* position: {x:number, y:number}

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the next turn occurs, always after the **update** event.

Content:
* current: PlayerId
* remainingTime: number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\
The game is considerated as ended when a player has aligned 4 pawns.\
It contains an array containing the positions of the winning pawns and a boolean indicates whether or not the player has won.

Content:
* pawns: Array<{x:number, y:number}>
* hasWon: boolean

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Send the position where the player wish to play.\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The pawn is not connected with the others pawns.*
* *The pawn overlap another pawn.*

Content:
* position: {x:number, y:number}

## Objects

### PlayerId

PlayerId is a simple string.